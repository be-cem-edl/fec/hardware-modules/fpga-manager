# SPDX-FileCopyrightText: 2023 2023 CERN (home.cern)
#
# SPDX-License-Identifier: CC0-1.0

-include Makefile.specific
-include $(REPO_PARENT)/parent_common.mk

all: modules

modules help install modules_install clean:
	export CONFIG_FPGA=y
	make -C $(shell pwd)/drivers/fpga $@

.PHONY: all modules clean help install modules_install
