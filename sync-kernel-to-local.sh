#!/bin/bash

# SPDX-FileCopyrightText: 2023 2023 CERN (home.cern)
#
# SPDX-License-Identifier: GPL-2.0-or-later

if [ "x$KERNELSRC" = "x" ]
then
    echo "Provide the path to the linux sources with the environment variable KERNELSRC"
    exit 1
fi

DOC=Documentation/fpga
INCLUDE=include/linux/fpga
DRIVERS=drivers/fpga

mkdir -p $DOC
mkdir -p $INCLUDE
mkdir -p $DRIVERS

rsync -avz $KERNELSRC/$DOC/* $DOC/
rsync -avz $KERNELSRC/$INCLUDE/* $INCLUDE/
rsync -avz $KERNELSRC/$DRIVERS/* $DRIVERS/
